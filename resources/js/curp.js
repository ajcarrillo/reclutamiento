import entidades from './entidades';
import Paises from './paises';

export default class Curp {
    constructor(curp) {
        this._curp = curp;
    }

    searchInRenapo() {
        let that = this;
        return new Promise(function (resolve, reject) {
            axios.get(`api/resolver-curp/${that._curp}`)
                .then(res => {
                    that._persona = res.data.persona;
                    that._setHistorica();
                    resolve(res);
                })
                .catch(err => {
                    console.log(err.response);
                    reject(err);
                })
        });
    }

    getEntidadNacimiento() {
        let that = this;

        return entidades.ENTIDADES.find(function (item) {
            return item.letras_curp === that._persona.cveEntidadNac;
        })
    }

    getNacionalidad() {
        let that = this;
        return Paises.find(function (item) {
            return item.iso3 === that._persona.nacionalidad
        })
    }

    getFechaNacimiento() {
        let fecha = this._persona.fechNac;

        return fecha.split('/').reverse().join('-');
    }

    getSexo() {
        return this._persona.sexo;
    }

    getStatus() {
        return this._persona.statusCurp;
    }

    getCodigo() {
        return this._persona.codigo;
    }

    _setHistorica() {
        this.esHistorica = this._persona.CURP !== this._curp;
    }

    getCurp() {
        return this._persona.CURP;
    }

    getEdad() {
        let fecha = moment(this.getFechaNacimiento());
        let hoy = moment();

        return hoy.diff(fecha, 'years');
    }
}

