export default {
    "ENTIDADES": [
        {
            "id": "01",
            "letras_curp": "AS",
            "nombre": "AGUASCALIENTES"
        },
        {
            "id": "02",
            "letras_curp": "BC",
            "nombre": "BAJA CALIFORNIA"
        },
        {
            "id": "03",
            "letras_curp": "BS",
            "nombre": "BAJA CALIFORNIA SUR"
        },
        {
            "id": "04",
            "letras_curp": "CC",
            "nombre": "CAMPECHE"
        },
        {
            "id": "05",
            "letras_curp": "CL",
            "nombre": "COAHUILA"
        },
        {
            "id": "06",
            "letras_curp": "CM",
            "nombre": "COLIMA"
        },
        {
            "id": "07",
            "letras_curp": "CS",
            "nombre": "CHIAPAS"
        },
        {
            "id": "08",
            "letras_curp": "CH",
            "nombre": "CHIHUAHUA"
        },
        {
            "id": "09",
            "letras_curp": "DF",
            "nombre": "DISTRITO FEDERAL"
        },
        {
            "id": "10",
            "letras_curp": "DG",
            "nombre": "DURANGO"
        },
        {
            "id": "11",
            "letras_curp": "GT",
            "nombre": "GUANAJUATO"
        },
        {
            "id": "12",
            "letras_curp": "GR",
            "nombre": "GUERRERO"
        },
        {
            "id": "13",
            "letras_curp": "HG",
            "nombre": "HIDALGO"
        },
        {
            "id": "14",
            "letras_curp": "JC",
            "nombre": "JALISCO"
        },
        {
            "id": "15",
            "letras_curp": "MC",
            "nombre": "MEXICO"
        },
        {
            "id": "16",
            "letras_curp": "MN",
            "nombre": "MICHOACAN"
        },
        {
            "id": "17",
            "letras_curp": "MS",
            "nombre": "MORELOS"
        },
        {
            "id": "18",
            "letras_curp": "NT",
            "nombre": "NAYARIT"
        },
        {
            "id": "19",
            "letras_curp": "NL",
            "nombre": "NUEVO LEON"
        },
        {
            "id": "20",
            "letras_curp": "OC",
            "nombre": "OAXACA"
        },
        {
            "id": "21",
            "letras_curp": "PL",
            "nombre": "PUEBLA"
        },
        {
            "id": "22",
            "letras_curp": "QT",
            "nombre": "QUERETARO"
        },
        {
            "id": "23",
            "letras_curp": "QR",
            "nombre": "QUINTANA ROO"
        },
        {
            "id": "24",
            "letras_curp": "SP",
            "nombre": "SAN LUIS POTOSI"
        },
        {
            "id": "25",
            "letras_curp": "SL",
            "nombre": "SINALOA"
        },
        {
            "id": "26",
            "letras_curp": "SR",
            "nombre": "SONORA"
        },
        {
            "id": "27",
            "letras_curp": "TC",
            "nombre": "TABASCO"
        },
        {
            "id": "28",
            "letras_curp": "TS",
            "nombre": "TAMAULIPAS"
        },
        {
            "id": "29",
            "letras_curp": "TL",
            "nombre": "TLAXCALA"
        },
        {
            "id": "30",
            "letras_curp": "VZ",
            "nombre": "VERACRUZ"
        },
        {
            "id": "31",
            "letras_curp": "YN",
            "nombre": "YUCATAN"
        },
        {
            "id": "32",
            "letras_curp": "ZS",
            "nombre": "ZACATECAS"
        },
        {
            "id": "33",
            "letras_curp": "NE",
            "nombre": "NACIDO EN EL EXTRANJERO"
        }
    ]
}
