export const tipoSangre = [
    {"O-": 'O NEGATIVO'},
    {"O+": 'O POSITIVO'},
    {"A-": 'A NEGATIVO'},
    {"A+": 'A POSITIVO'},
    {"B-": 'B NEGATIVO'},
    {"B+": 'B POSITIVO'},
    {"AB-": 'AB NEGATIVO'},
    {"AB+": 'AB POSITIVO'},
];
