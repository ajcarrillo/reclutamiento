import Vue from 'vue'
import VueRouter from 'vue-router';
import routes from './routes';
import store from "../store/store";

Vue.use(VueRouter);

const router = new VueRouter(routes);

router.beforeEach(async (to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters['auth/token']) {

            if (!store.getters['auth/check']) {
                await store.dispatch('auth/fetchUser');
            }

            next();
            return
        }
        next('/login');
    } else {
        next()
    }
});

export default router
