import Home from '@/pages/HomeComponent';
import Login from '@/pages/Login';
import Perfil from '@/pages/aspirante/Perfil';
import Register from '@/components/auth/RegisterComponent';
import NotFound from '@/pages/NotFound';

import store from '@/store/store'

export default {
    mode: 'history',
    routes: [
        {
            path: '*',
            component: NotFound
        },
        {
            path: '/',
            component: Home
        },
        {
            path: '/login',
            component: Login
        },
        {
            path: '/registro',
            component: Register,
        },
        {
            path: '/perfil',
            name: 'profile',
            component: Perfil,
            beforeEnter: (to, from, next) => {
                if (!store.getters['aspirante/hasDatos']) {
                    let uuid = store.getters['auth/user'].uuid;
                    store.dispatch('aspirante/fetchDatosGenerales', uuid)
                        .then(res => {
                            next();
                            return;
                        });
                }
                next();
            },
            meta: {
                requiresAuth: true
            }
        },

    ]
}
