require('./bootstrap');

window.Vue = require('vue');

window.clone = function (obj) {
    return JSON.parse(JSON.stringify(obj));
};

import VModal from 'vue-js-modal'
import Vuelidate from 'vuelidate'
import LaravelPermissions from 'laravel-permissions';

import App from './components/App';
import store from './store/store';
import router from './router';
import './filters';

Vue.use(VModal);
Vue.use(Vuelidate);
Vue.use(LaravelPermissions, {persistent: true});

const app = new Vue({
    store,
    router,
    ...App
});
