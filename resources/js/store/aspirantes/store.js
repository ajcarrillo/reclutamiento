import actions from './actions';
import getters from './getters'
import mutations from './mutations'

export default {
    namespaced: true,
    state: {
        datosGenerales: null,
    },
    actions,
    getters,
    mutations
}
