export default {
    hasDatos: state => state.datosGenerales != null,
    getDatos: state => state.datosGenerales,
    datosGenerales: state => state.datosGenerales,
}
