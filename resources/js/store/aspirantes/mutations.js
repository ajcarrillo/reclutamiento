export default {
    SET_DATOS_GEN(state, payload) {
        state.datosGenerales = payload.user.datos_generales;
    },
    SET_RFC(state, payload) {
        state.datosGenerales.rfc = payload;
    },
    UPDATE_DATOS_GEN(state, payload) {
        state.datosGenerales.tipo_sangre = payload.tipo_sangre;
        state.datosGenerales.estado_civil_id = payload.estado_civil_id;
        state.datosGenerales.tiene_hijos = payload.tiene_hijos;
        state.datosGenerales.numero_hijos = payload.numero_hijos;
        state.datosGenerales.puede_cambiar_residencia = payload.puede_cambiar_residencia;
        state.datosGenerales.labora_actualmente = payload.labora_actualmente;
        state.datosGenerales.aspiracion_salarial = payload.aspiracion_salarial;
        state.datosGenerales.trabaja_honorarios = payload.trabaja_honorarios;
        state.datosGenerales.maximo_grado_estudios = payload.maximo_grado_estudios;
    }
}
