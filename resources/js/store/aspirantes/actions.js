export default {
    fetchDatosGenerales(context, payload) {
        return new Promise(function (resolve, reject) {
            axios.get(route('api.aspirantes.show', payload))
                .then(res => {
                    let payload = res.data;
                    context.commit('SET_DATOS_GEN', payload);
                    resolve(res);
                })
                .catch(err => {
                    console.log(err);
                    reject(err);
                })
        })
    },
    updateRfc(context, payload) {
        return new Promise(function (resolve, reject) {
            axios.patch(route('api.aspirantes.update.rfc', payload.id), {
                rfc: payload.rfc
            })
                .then(res => {
                    context.commit('SET_RFC', res.data.rfc);
                    resolve(res);
                })
                .catch(err => {
                    console.log(err);
                    reject(err);
                })
        })
    },
    updateDatosGenerales(context, payload) {
        return new Promise(function (resolve, reject) {
            axios.patch(route('api.aspirantes.update.datos', payload.id), {
                tipo_sangre: payload.tipo_sangre,
                estado_civil_id: payload.estado_civil_id,
                tiene_hijos: payload.tiene_hijos,
                numero_hijos: payload.numero_hijos,
                puede_cambiar_residencia: payload.puede_cambiar_residencia,
                labora_actualmente: payload.labora_actualmente,
                aspiracion_salarial: payload.aspiracion_salarial,
                trabaja_honorarios: payload.trabaja_honorarios,
                maximo_grado_estudios: payload.maximo_grado_estudios,
            })
                .then(res => {
                    context.commit('aspirante/UPDATE_DATOS_GEN', res.data.datos_generales);
                    resolve(res);
                })
                .catch(err => {
                    console.log(err);
                    reject(err);
                })
        })
    }
}
