import Vue from 'vue'
import Vuex from 'vuex'
import Auth from './auth/store'
import Aspirante from './aspirantes/store';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        auth: Auth,
        aspirante: Aspirante
    }
})
