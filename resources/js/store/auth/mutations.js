export default {
    SET_USER(state, {user, token, roles, permissions}) {
        state.isLogin = true;
        state.user = user;
        state.permissions = permissions;
        state.roles = roles;
        state.token = token;
    },
    LOGIN(state, payload) {
    },
    LOGOUT(state, payload) {
    }
}
