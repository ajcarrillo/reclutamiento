export default {
    register(context, payload) {
        return new Promise(function (resolve, reject) {
            axios.post(route('api.auth.register'), payload)
                .then(res => {

                    let user = res.data.user;
                    let token = user.api_token;
                    let roles = res.data.roles;
                    let permissions = res.data.permissions;

                    context.commit('SET_USER', {user, token, roles, permissions});

                    localStorage.setItem('token', JSON.stringify(token));
                    localStorage.setItem('user', JSON.stringify(user));

                    resolve(res);

                })
                .catch(err => {
                    console.log(err);
                    reject(err.response);
                })
        })
    },
    async fetchUser(context) {
        try {
            const {data} = await axios.get(route('api.user'));

            let user = data.user;
            let token = user.api_token;
            let roles = data.roles;
            let permissions = data.permissions;

            context.commit('SET_USER', {user, token, roles, permissions});

            localStorage.setItem('token', JSON.stringify(token));
            localStorage.setItem('user', JSON.stringify(user));
        } catch (e) {

        }
    },
    logIn(context, payload) {
    },
    logOut(context, payload) {

    }
}
