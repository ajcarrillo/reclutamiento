export default {
    isLoggedIn: (state) => {
        return state.isLogin
    },
    check: state => state.user !== null,
    user: state => state.user,
    token: state => state.token,
}
