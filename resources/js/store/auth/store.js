import actions from './actions';
import getters from './getters'
import mutations from './mutations'

export default {
    namespaced: true,
    state: {
        isLogin: false,
        user: null,
        permissions: [],
        roles: [],
        token: JSON.parse(localStorage.getItem('token')) || '',
    },
    actions,
    getters,
    mutations
}
