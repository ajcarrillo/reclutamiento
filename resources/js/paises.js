export default [
    {
        "iso2": "AF",
        "iso3": "AFG",
        "nombre": "AFGANISTÁN"
    },
    {
        "iso2": "AL",
        "iso3": "ALB",
        "nombre": "ALBANIA"
    },
    {
        "iso2": "DE",
        "iso3": "DEU",
        "nombre": "ALEMANIA"
    },
    {
        "iso2": "DZ",
        "iso3": "DZA",
        "nombre": "ALGERIA"
    },
    {
        "iso2": "AD",
        "iso3": "AND",
        "nombre": "ANDORRA"
    },
    {
        "iso2": "AO",
        "iso3": "AGO",
        "nombre": "ANGOLA"
    },
    {
        "iso2": "AI",
        "iso3": "AIA",
        "nombre": "ANGUILA"
    },
    {
        "iso2": "AQ",
        "iso3": "ATA",
        "nombre": "ANTÁRTIDA"
    },
    {
        "iso2": "AG",
        "iso3": "ATG",
        "nombre": "ANTIGUA Y BARBUDA"
    },
    {
        "iso2": "AN",
        "iso3": "ANT",
        "nombre": "ANTILLAS NEERLANDESAS"
    },
    {
        "iso2": "SA",
        "iso3": "SAU",
        "nombre": "ARABIA SAUDITA"
    },
    {
        "iso2": "AR",
        "iso3": "ARG",
        "nombre": "ARGENTINA"
    },
    {
        "iso2": "AM",
        "iso3": "ARM",
        "nombre": "ARMENIA"
    },
    {
        "iso2": "AW",
        "iso3": "ABW",
        "nombre": "ARUBA"
    },
    {
        "iso2": "AU",
        "iso3": "AUS",
        "nombre": "AUSTRALIA"
    },
    {
        "iso2": "AT",
        "iso3": "AUT",
        "nombre": "AUSTRIA"
    },
    {
        "iso2": "AZ",
        "iso3": "AZE",
        "nombre": "AZERBAYÁN"
    },
    {
        "iso2": "BE",
        "iso3": "BEL",
        "nombre": "BÉLGICA"
    },
    {
        "iso2": "BS",
        "iso3": "BHS",
        "nombre": "BAHAMAS"
    },
    {
        "iso2": "BH",
        "iso3": "BHR",
        "nombre": "BAHREIN"
    },
    {
        "iso2": "BD",
        "iso3": "BGD",
        "nombre": "BANGLADESH"
    },
    {
        "iso2": "BB",
        "iso3": "BRB",
        "nombre": "BARBADOS"
    },
    {
        "iso2": "BZ",
        "iso3": "BLZ",
        "nombre": "BELICE"
    },
    {
        "iso2": "BJ",
        "iso3": "BEN",
        "nombre": "BENÍN"
    },
    {
        "iso2": "BT",
        "iso3": "BTN",
        "nombre": "BHUTÁN"
    },
    {
        "iso2": "BY",
        "iso3": "BLR",
        "nombre": "BIELORRUSIA"
    },
    {
        "iso2": "MM",
        "iso3": "MMR",
        "nombre": "BIRMANIA"
    },
    {
        "iso2": "BO",
        "iso3": "BOL",
        "nombre": "BOLIVIA"
    },
    {
        "iso2": "BA",
        "iso3": "BIH",
        "nombre": "BOSNIA Y HERZEGOVINA"
    },
    {
        "iso2": "BW",
        "iso3": "BWA",
        "nombre": "BOTSUANA"
    },
    {
        "iso2": "BR",
        "iso3": "BRA",
        "nombre": "BRASIL"
    },
    {
        "iso2": "BN",
        "iso3": "BRN",
        "nombre": "BRUNÉI"
    },
    {
        "iso2": "BG",
        "iso3": "BGR",
        "nombre": "BULGARIA"
    },
    {
        "iso2": "BF",
        "iso3": "BFA",
        "nombre": "BURKINA FASO"
    },
    {
        "iso2": "BI",
        "iso3": "BDI",
        "nombre": "BURUNDI"
    },
    {
        "iso2": "CV",
        "iso3": "CPV",
        "nombre": "CABO VERDE"
    },
    {
        "iso2": "KH",
        "iso3": "KHM",
        "nombre": "CAMBOYA"
    },
    {
        "iso2": "CM",
        "iso3": "CMR",
        "nombre": "CAMERÚN"
    },
    {
        "iso2": "CA",
        "iso3": "CAN",
        "nombre": "CANADÁ"
    },
    {
        "iso2": "TD",
        "iso3": "TCD",
        "nombre": "CHAD"
    },
    {
        "iso2": "CL",
        "iso3": "CHL",
        "nombre": "CHILE"
    },
    {
        "iso2": "CN",
        "iso3": "CHN",
        "nombre": "CHINA"
    },
    {
        "iso2": "CY",
        "iso3": "CYP",
        "nombre": "CHIPRE"
    },
    {
        "iso2": "VA",
        "iso3": "VAT",
        "nombre": "CIUDAD DEL VATICANO"
    },
    {
        "iso2": "CO",
        "iso3": "COL",
        "nombre": "COLOMBIA"
    },
    {
        "iso2": "KM",
        "iso3": "COM",
        "nombre": "COMORAS"
    },
    {
        "iso2": "CG",
        "iso3": "COG",
        "nombre": "CONGO"
    },
    {
        "iso2": "CD",
        "iso3": "COD",
        "nombre": "CONGO"
    },
    {
        "iso2": "KP",
        "iso3": "PRK",
        "nombre": "COREA DEL NORTE"
    },
    {
        "iso2": "KR",
        "iso3": "KOR",
        "nombre": "COREA DEL SUR"
    },
    {
        "iso2": "CI",
        "iso3": "CIV",
        "nombre": "COSTA DE MARFIL"
    },
    {
        "iso2": "CR",
        "iso3": "CRI",
        "nombre": "COSTA RICA"
    },
    {
        "iso2": "HR",
        "iso3": "HRV",
        "nombre": "CROACIA"
    },
    {
        "iso2": "CU",
        "iso3": "CUB",
        "nombre": "CUBA"
    },
    {
        "iso2": "DK",
        "iso3": "DNK",
        "nombre": "DINAMARCA"
    },
    {
        "iso2": "DM",
        "iso3": "DMA",
        "nombre": "DOMINICA"
    },
    {
        "iso2": "EC",
        "iso3": "ECU",
        "nombre": "ECUADOR"
    },
    {
        "iso2": "EG",
        "iso3": "EGY",
        "nombre": "EGIPTO"
    },
    {
        "iso2": "SV",
        "iso3": "SLV",
        "nombre": "EL SALVADOR"
    },
    {
        "iso2": "AE",
        "iso3": "ARE",
        "nombre": "EMIRATOS ÁRABES UNIDOS"
    },
    {
        "iso2": "ER",
        "iso3": "ERI",
        "nombre": "ERITREA"
    },
    {
        "iso2": "SK",
        "iso3": "SVK",
        "nombre": "ESLOVAQUIA"
    },
    {
        "iso2": "SI",
        "iso3": "SVN",
        "nombre": "ESLOVENIA"
    },
    {
        "iso2": "ES",
        "iso3": "ESP",
        "nombre": "ESPAÑA"
    },
    {
        "iso2": "US",
        "iso3": "USA",
        "nombre": "ESTADOS UNIDOS DE AMÉRICA"
    },
    {
        "iso2": "EE",
        "iso3": "EST",
        "nombre": "ESTONIA"
    },
    {
        "iso2": "ET",
        "iso3": "ETH",
        "nombre": "ETIOPÍA"
    },
    {
        "iso2": "PH",
        "iso3": "PHL",
        "nombre": "FILIPINAS"
    },
    {
        "iso2": "FI",
        "iso3": "FIN",
        "nombre": "FINLANDIA"
    },
    {
        "iso2": "FJ",
        "iso3": "FJI",
        "nombre": "FIYI"
    },
    {
        "iso2": "FR",
        "iso3": "FRA",
        "nombre": "FRANCIA"
    },
    {
        "iso2": "GA",
        "iso3": "GAB",
        "nombre": "GABÓN"
    },
    {
        "iso2": "GM",
        "iso3": "GMB",
        "nombre": "GAMBIA"
    },
    {
        "iso2": "GE",
        "iso3": "GEO",
        "nombre": "GEORGIA"
    },
    {
        "iso2": "GH",
        "iso3": "GHA",
        "nombre": "GHANA"
    },
    {
        "iso2": "GI",
        "iso3": "GIB",
        "nombre": "GIBRALTAR"
    },
    {
        "iso2": "GD",
        "iso3": "GRD",
        "nombre": "GRANADA"
    },
    {
        "iso2": "GR",
        "iso3": "GRC",
        "nombre": "GRECIA"
    },
    {
        "iso2": "GL",
        "iso3": "GRL",
        "nombre": "GROENLANDIA"
    },
    {
        "iso2": "GP",
        "iso3": "GLP",
        "nombre": "GUADALUPE"
    },
    {
        "iso2": "GU",
        "iso3": "GUM",
        "nombre": "GUAM"
    },
    {
        "iso2": "GT",
        "iso3": "GTM",
        "nombre": "GUATEMALA"
    },
    {
        "iso2": "GF",
        "iso3": "GUF",
        "nombre": "GUAYANA FRANCESA"
    },
    {
        "iso2": "GG",
        "iso3": "GGY",
        "nombre": "GUERNSEY"
    },
    {
        "iso2": "GN",
        "iso3": "GIN",
        "nombre": "GUINEA"
    },
    {
        "iso2": "GQ",
        "iso3": "GNQ",
        "nombre": "GUINEA ECUATORIAL"
    },
    {
        "iso2": "GW",
        "iso3": "GNB",
        "nombre": "GUINEA-BISSAU"
    },
    {
        "iso2": "GY",
        "iso3": "GUY",
        "nombre": "GUYANA"
    },
    {
        "iso2": "HT",
        "iso3": "HTI",
        "nombre": "HAITÍ"
    },
    {
        "iso2": "HN",
        "iso3": "HND",
        "nombre": "HONDURAS"
    },
    {
        "iso2": "HK",
        "iso3": "HKG",
        "nombre": "HONG KONG"
    },
    {
        "iso2": "HU",
        "iso3": "HUN",
        "nombre": "HUNGRÍA"
    },
    {
        "iso2": "IN",
        "iso3": "IND",
        "nombre": "INDIA"
    },
    {
        "iso2": "ID",
        "iso3": "IDN",
        "nombre": "INDONESIA"
    },
    {
        "iso2": "IR",
        "iso3": "IRN",
        "nombre": "IRÁN"
    },
    {
        "iso2": "IQ",
        "iso3": "IRQ",
        "nombre": "IRAK"
    },
    {
        "iso2": "IE",
        "iso3": "IRL",
        "nombre": "IRLANDA"
    },
    {
        "iso2": "BV",
        "iso3": "BVT",
        "nombre": "ISLA BOUVET"
    },
    {
        "iso2": "IM",
        "iso3": "IMN",
        "nombre": "ISLA DE MAN"
    },
    {
        "iso2": "CX",
        "iso3": "CXR",
        "nombre": "ISLA DE NAVIDAD"
    },
    {
        "iso2": "NF",
        "iso3": "NFK",
        "nombre": "ISLA NORFOLK"
    },
    {
        "iso2": "IS",
        "iso3": "ISL",
        "nombre": "ISLANDIA"
    },
    {
        "iso2": "BM",
        "iso3": "BMU",
        "nombre": "ISLAS BERMUDAS"
    },
    {
        "iso2": "KY",
        "iso3": "CYM",
        "nombre": "ISLAS CAIMÁN"
    },
    {
        "iso2": "CC",
        "iso3": "CCK",
        "nombre": "ISLAS COCOS (KEELING)"
    },
    {
        "iso2": "CK",
        "iso3": "COK",
        "nombre": "ISLAS COOK"
    },
    {
        "iso2": "AX",
        "iso3": "ALA",
        "nombre": "ISLAS DE ÅLAND"
    },
    {
        "iso2": "FO",
        "iso3": "FRO",
        "nombre": "ISLAS FEROE"
    },
    {
        "iso2": "GS",
        "iso3": "SGS",
        "nombre": "ISLAS GEORGIAS DEL SUR Y SANDWICH DEL SUR"
    },
    {
        "iso2": "HM",
        "iso3": "HMD",
        "nombre": "ISLAS HEARD Y MCDONALD"
    },
    {
        "iso2": "MV",
        "iso3": "MDV",
        "nombre": "ISLAS MALDIVAS"
    },
    {
        "iso2": "FK",
        "iso3": "FLK",
        "nombre": "ISLAS MALVINAS"
    },
    {
        "iso2": "MP",
        "iso3": "MNP",
        "nombre": "ISLAS MARIANAS DEL NORTE"
    },
    {
        "iso2": "MH",
        "iso3": "MHL",
        "nombre": "ISLAS MARSHALL"
    },
    {
        "iso2": "PN",
        "iso3": "PCN",
        "nombre": "ISLAS PITCAIRN"
    },
    {
        "iso2": "SB",
        "iso3": "SLB",
        "nombre": "ISLAS SALOMÓN"
    },
    {
        "iso2": "TC",
        "iso3": "TCA",
        "nombre": "ISLAS TURCAS Y CAICOS"
    },
    {
        "iso2": "UM",
        "iso3": "UMI",
        "nombre": "ISLAS ULTRAMARINAS MENORES DE ESTADOS UNIDOS"
    },
    {
        "iso2": "VG",
        "iso3": "VG",
        "nombre": "ISLAS VÍRGENES BRITÁNICAS"
    },
    {
        "iso2": "VI",
        "iso3": "VIR",
        "nombre": "ISLAS VÍRGENES DE LOS ESTADOS UNIDOS"
    },
    {
        "iso2": "IL",
        "iso3": "ISR",
        "nombre": "ISRAEL"
    },
    {
        "iso2": "IT",
        "iso3": "ITA",
        "nombre": "ITALIA"
    },
    {
        "iso2": "JM",
        "iso3": "JAM",
        "nombre": "JAMAICA"
    },
    {
        "iso2": "JP",
        "iso3": "JPN",
        "nombre": "JAPÓN"
    },
    {
        "iso2": "JE",
        "iso3": "JEY",
        "nombre": "JERSEY"
    },
    {
        "iso2": "JO",
        "iso3": "JOR",
        "nombre": "JORDANIA"
    },
    {
        "iso2": "KZ",
        "iso3": "KAZ",
        "nombre": "KAZAJISTÁN"
    },
    {
        "iso2": "KE",
        "iso3": "KEN",
        "nombre": "KENIA"
    },
    {
        "iso2": "KG",
        "iso3": "KGZ",
        "nombre": "KIRGIZSTÁN"
    },
    {
        "iso2": "KI",
        "iso3": "KIR",
        "nombre": "KIRIBATI"
    },
    {
        "iso2": "KW",
        "iso3": "KWT",
        "nombre": "KUWAIT"
    },
    {
        "iso2": "LB",
        "iso3": "LBN",
        "nombre": "LÍBANO"
    },
    {
        "iso2": "LA",
        "iso3": "LAO",
        "nombre": "LAOS"
    },
    {
        "iso2": "LS",
        "iso3": "LSO",
        "nombre": "LESOTO"
    },
    {
        "iso2": "LV",
        "iso3": "LVA",
        "nombre": "LETONIA"
    },
    {
        "iso2": "LR",
        "iso3": "LBR",
        "nombre": "LIBERIA"
    },
    {
        "iso2": "LY",
        "iso3": "LBY",
        "nombre": "LIBIA"
    },
    {
        "iso2": "LI",
        "iso3": "LIE",
        "nombre": "LIECHTENSTEIN"
    },
    {
        "iso2": "LT",
        "iso3": "LTU",
        "nombre": "LITUANIA"
    },
    {
        "iso2": "LU",
        "iso3": "LUX",
        "nombre": "LUXEMBURGO"
    },
    {
        "iso2": "MX",
        "iso3": "MEX",
        "nombre": "MÉXICO"
    },
    {
        "iso2": "MC",
        "iso3": "MCO",
        "nombre": "MÓNACO"
    },
    {
        "iso2": "MO",
        "iso3": "MAC",
        "nombre": "MACAO"
    },
    {
        "iso2": "MK",
        "iso3": "MKD",
        "nombre": "MACEDÔNIA"
    },
    {
        "iso2": "MG",
        "iso3": "MDG",
        "nombre": "MADAGASCAR"
    },
    {
        "iso2": "MY",
        "iso3": "MYS",
        "nombre": "MALASIA"
    },
    {
        "iso2": "MW",
        "iso3": "MWI",
        "nombre": "MALAWI"
    },
    {
        "iso2": "ML",
        "iso3": "MLI",
        "nombre": "MALI"
    },
    {
        "iso2": "MT",
        "iso3": "MLT",
        "nombre": "MALTA"
    },
    {
        "iso2": "MA",
        "iso3": "MAR",
        "nombre": "MARRUECOS"
    },
    {
        "iso2": "MQ",
        "iso3": "MTQ",
        "nombre": "MARTINICA"
    },
    {
        "iso2": "MU",
        "iso3": "MUS",
        "nombre": "MAURICIO"
    },
    {
        "iso2": "MR",
        "iso3": "MRT",
        "nombre": "MAURITANIA"
    },
    {
        "iso2": "YT",
        "iso3": "MYT",
        "nombre": "MAYOTTE"
    },
    {
        "iso2": "FM",
        "iso3": "FSM",
        "nombre": "MICRONESIA"
    },
    {
        "iso2": "MD",
        "iso3": "MDA",
        "nombre": "MOLDAVIA"
    },
    {
        "iso2": "MN",
        "iso3": "MNG",
        "nombre": "MONGOLIA"
    },
    {
        "iso2": "ME",
        "iso3": "MNE",
        "nombre": "MONTENEGRO"
    },
    {
        "iso2": "MS",
        "iso3": "MSR",
        "nombre": "MONTSERRAT"
    },
    {
        "iso2": "MZ",
        "iso3": "MOZ",
        "nombre": "MOZAMBIQUE"
    },
    {
        "iso2": "NA",
        "iso3": "NAM",
        "nombre": "NAMIBIA"
    },
    {
        "iso2": "NR",
        "iso3": "NRU",
        "nombre": "NAURU"
    },
    {
        "iso2": "NP",
        "iso3": "NPL",
        "nombre": "NEPAL"
    },
    {
        "iso2": "NI",
        "iso3": "NIC",
        "nombre": "NICARAGUA"
    },
    {
        "iso2": "NE",
        "iso3": "NER",
        "nombre": "NIGER"
    },
    {
        "iso2": "NG",
        "iso3": "NGA",
        "nombre": "NIGERIA"
    },
    {
        "iso2": "NU",
        "iso3": "NIU",
        "nombre": "NIUE"
    },
    {
        "iso2": "NO",
        "iso3": "NOR",
        "nombre": "NORUEGA"
    },
    {
        "iso2": "NC",
        "iso3": "NCL",
        "nombre": "NUEVA CALEDONIA"
    },
    {
        "iso2": "NZ",
        "iso3": "NZL",
        "nombre": "NUEVA ZELANDA"
    },
    {
        "iso2": "OM",
        "iso3": "OMN",
        "nombre": "OMÁN"
    },
    {
        "iso2": "NL",
        "iso3": "NLD",
        "nombre": "PAÍSES BAJOS"
    },
    {
        "iso2": "PK",
        "iso3": "PAK",
        "nombre": "PAKISTÁN"
    },
    {
        "iso2": "PW",
        "iso3": "PLW",
        "nombre": "PALAU"
    },
    {
        "iso2": "PS",
        "iso3": "PSE",
        "nombre": "PALESTINA"
    },
    {
        "iso2": "PA",
        "iso3": "PAN",
        "nombre": "PANAMÁ"
    },
    {
        "iso2": "PG",
        "iso3": "PNG",
        "nombre": "PAPÚA NUEVA GUINEA"
    },
    {
        "iso2": "PY",
        "iso3": "PRY",
        "nombre": "PARAGUAY"
    },
    {
        "iso2": "PE",
        "iso3": "PER",
        "nombre": "PERÚ"
    },
    {
        "iso2": "PF",
        "iso3": "PYF",
        "nombre": "POLINESIA FRANCESA"
    },
    {
        "iso2": "PL",
        "iso3": "POL",
        "nombre": "POLONIA"
    },
    {
        "iso2": "PT",
        "iso3": "PRT",
        "nombre": "PORTUGAL"
    },
    {
        "iso2": "PR",
        "iso3": "PRI",
        "nombre": "PUERTO RICO"
    },
    {
        "iso2": "QA",
        "iso3": "QAT",
        "nombre": "QATAR"
    },
    {
        "iso2": "GB",
        "iso3": "GBR",
        "nombre": "REINO UNIDO"
    },
    {
        "iso2": "CF",
        "iso3": "CAF",
        "nombre": "REPÚBLICA CENTROAFRICANA"
    },
    {
        "iso2": "CZ",
        "iso3": "CZE",
        "nombre": "REPÚBLICA CHECA"
    },
    {
        "iso2": "DO",
        "iso3": "DOM",
        "nombre": "REPÚBLICA DOMINICANA"
    },
    {
        "iso2": "RE",
        "iso3": "REU",
        "nombre": "REUNIÓN"
    },
    {
        "iso2": "RW",
        "iso3": "RWA",
        "nombre": "RUANDA"
    },
    {
        "iso2": "RO",
        "iso3": "ROU",
        "nombre": "RUMANÍA"
    },
    {
        "iso2": "RU",
        "iso3": "RUS",
        "nombre": "RUSIA"
    },
    {
        "iso2": "EH",
        "iso3": "ESH",
        "nombre": "SAHARA OCCIDENTAL"
    },
    {
        "iso2": "WS",
        "iso3": "WSM",
        "nombre": "SAMOA"
    },
    {
        "iso2": "AS",
        "iso3": "ASM",
        "nombre": "SAMOA AMERICANA"
    },
    {
        "iso2": "BL",
        "iso3": "BLM",
        "nombre": "SAN BARTOLOMÉ"
    },
    {
        "iso2": "KN",
        "iso3": "KNA",
        "nombre": "SAN CRISTÓBAL Y NIEVES"
    },
    {
        "iso2": "SM",
        "iso3": "SMR",
        "nombre": "SAN MARINO"
    },
    {
        "iso2": "MF",
        "iso3": "MAF",
        "nombre": "SAN MARTÍN (FRANCIA)"
    },
    {
        "iso2": "PM",
        "iso3": "SPM",
        "nombre": "SAN PEDRO Y MIQUELÓN"
    },
    {
        "iso2": "VC",
        "iso3": "VCT",
        "nombre": "SAN VICENTE Y LAS GRANADINAS"
    },
    {
        "iso2": "SH",
        "iso3": "SHN",
        "nombre": "SANTA ELENA"
    },
    {
        "iso2": "LC",
        "iso3": "LCA",
        "nombre": "SANTA LUCÍA"
    },
    {
        "iso2": "ST",
        "iso3": "STP",
        "nombre": "SANTO TOMÉ Y PRÍNCIPE"
    },
    {
        "iso2": "SN",
        "iso3": "SEN",
        "nombre": "SENEGAL"
    },
    {
        "iso2": "RS",
        "iso3": "SRB",
        "nombre": "SERBIA"
    },
    {
        "iso2": "SC",
        "iso3": "SYC",
        "nombre": "SEYCHELLES"
    },
    {
        "iso2": "SL",
        "iso3": "SLE",
        "nombre": "SIERRA LEONA"
    },
    {
        "iso2": "SG",
        "iso3": "SGP",
        "nombre": "SINGAPUR"
    },
    {
        "iso2": "SY",
        "iso3": "SYR",
        "nombre": "SIRIA"
    },
    {
        "iso2": "SO",
        "iso3": "SOM",
        "nombre": "SOMALIA"
    },
    {
        "iso2": "LK",
        "iso3": "LKA",
        "nombre": "SRI LANKA"
    },
    {
        "iso2": "ZA",
        "iso3": "ZAF",
        "nombre": "SUDÁFRICA"
    },
    {
        "iso2": "SD",
        "iso3": "SDN",
        "nombre": "SUDÁN"
    },
    {
        "iso2": "SE",
        "iso3": "SWE",
        "nombre": "SUECIA"
    },
    {
        "iso2": "CH",
        "iso3": "CHE",
        "nombre": "SUIZA"
    },
    {
        "iso2": "SR",
        "iso3": "SUR",
        "nombre": "SURINÁM"
    },
    {
        "iso2": "SJ",
        "iso3": "SJM",
        "nombre": "SVALBARD Y JAN MAYEN"
    },
    {
        "iso2": "SZ",
        "iso3": "SWZ",
        "nombre": "SWAZILANDIA"
    },
    {
        "iso2": "TJ",
        "iso3": "TJK",
        "nombre": "TADJIKISTÁN"
    },
    {
        "iso2": "TH",
        "iso3": "THA",
        "nombre": "TAILANDIA"
    },
    {
        "iso2": "TW",
        "iso3": "TWN",
        "nombre": "TAIWÁN"
    },
    {
        "iso2": "TZ",
        "iso3": "TZA",
        "nombre": "TANZANIA"
    },
    {
        "iso2": "IO",
        "iso3": "IOT",
        "nombre": "TERRITORIO BRITÁNICO DEL OCÉANO ÍNDICO"
    },
    {
        "iso2": "TF",
        "iso3": "ATF",
        "nombre": "TERRITORIOS AUSTRALES Y ANTÁRTICAS FRANCESES"
    },
    {
        "iso2": "TL",
        "iso3": "TLS",
        "nombre": "TIMOR ORIENTAL"
    },
    {
        "iso2": "TG",
        "iso3": "TGO",
        "nombre": "TOGO"
    },
    {
        "iso2": "TK",
        "iso3": "TKL",
        "nombre": "TOKELAU"
    },
    {
        "iso2": "TO",
        "iso3": "TON",
        "nombre": "TONGA"
    },
    {
        "iso2": "TT",
        "iso3": "TTO",
        "nombre": "TRINIDAD Y TOBAGO"
    },
    {
        "iso2": "TN",
        "iso3": "TUN",
        "nombre": "TUNEZ"
    },
    {
        "iso2": "TM",
        "iso3": "TKM",
        "nombre": "TURKMENISTÁN"
    },
    {
        "iso2": "TR",
        "iso3": "TUR",
        "nombre": "TURQUÍA"
    },
    {
        "iso2": "TV",
        "iso3": "TUV",
        "nombre": "TUVALU"
    },
    {
        "iso2": "UA",
        "iso3": "UKR",
        "nombre": "UCRANIA"
    },
    {
        "iso2": "UG",
        "iso3": "UGA",
        "nombre": "UGANDA"
    },
    {
        "iso2": "UY",
        "iso3": "URY",
        "nombre": "URUGUAY"
    },
    {
        "iso2": "UZ",
        "iso3": "UZB",
        "nombre": "UZBEKISTÁN"
    },
    {
        "iso2": "VU",
        "iso3": "VUT",
        "nombre": "VANUATU"
    },
    {
        "iso2": "VE",
        "iso3": "VEN",
        "nombre": "VENEZUELA"
    },
    {
        "iso2": "VN",
        "iso3": "VNM",
        "nombre": "VIETNAM"
    },
    {
        "iso2": "WF",
        "iso3": "WLF",
        "nombre": "WALLIS Y FUTUNA"
    },
    {
        "iso2": "YE",
        "iso3": "YEM",
        "nombre": "YEMEN"
    },
    {
        "iso2": "DJ",
        "iso3": "DJI",
        "nombre": "YIBUTI"
    },
    {
        "iso2": "ZM",
        "iso3": "ZMB",
        "nombre": "ZAMBIA"
    },
    {
        "iso2": "ZW",
        "iso3": "ZWE",
        "nombre": "ZIMBABUE"
    }
]
