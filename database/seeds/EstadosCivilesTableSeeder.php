<?php

use Illuminate\Database\Seeder;

class EstadosCivilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            'CASADA',
            'CASADO',
            'CONCUBINATO',
            'DIVORCIADA',
            'DIVORCIADO',
            'SOLTERA',
            'SOLTERO',
            'VIUDA',
            'VIUDO',
            'UNIÓN LIBRE',
        ];

        foreach ($items as $item) {
            \Aspirantes\Models\EstadoCivil::create([ 'descripcion' => $item ]);
        }
    }
}
