<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid')->unique();
            $table->string('nombre');
            $table->string('primer_apellido');
            $table->string('segundo_apellido')->nullable();
            $table->string('nombre_completo', 765);
            $table->string('email')->unique();
            $table->string('password');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('api_token', 60)->unique();
            $table->string('session_id')->nullable();
            $table->boolean('active')->default(1);
            $table->string('provider_id')->nullable()->comment('Uuid de la tabla users de la base de datos de jarvis');
            $table->string('provider')->nullable();
            $table->text('jarvis_user_access_token')->nullable();
            $table->string('jarvis_user_token_type')->nullable();
            $table->integer('jarvis_user_token_expires_in')->nullable();
            $table->text('jarvis_user_refresh_token')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
