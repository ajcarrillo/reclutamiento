<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDomiciliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domicilios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->morphs('addressable');
            $table->string('clave_entidad', 2);
            $table->string('clave_municipio', 3);
            $table->string('clave_localidad', 4);
            $table->string('entidad');
            $table->string('municipio');
            $table->string('localidad');
            $table->string('calle');
            $table->string('entre_calle');
            $table->string('y_calle')->nullable();
            $table->string('numero');
            $table->string('codigo_postal');
            $table->string('referencias')->nullable();
            $table->boolean('domicilio_actual');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domicilios');
    }
}
