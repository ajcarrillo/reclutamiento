<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatosGeneralesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datos_generales', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('curp', 18);
            $table->string('rfc', 13)->nullable();
            $table->string('edad', 2);
            $table->date('fecha_nacimiento');
            $table->string('entidad_nacimiento')->index();
            $table->string('sexo', 1);
            $table->string('tipo_sangre', 3)->nullable();
            $table->unsignedTinyInteger('estado_civil_id')->nullable();
            $table->foreign('estado_civil_id')->references('id')->on('estados_civiles');
            $table->boolean('tiene_hijos')->nullable();
            $table->tinyInteger('numero_hijos')->nullable();
            $table->boolean('puede_cambiar_residencia')->nullable();
            $table->boolean('labora_actualmente')->nullable();
            $table->decimal('aspiracion_salarial')->nullable();
            $table->boolean('trabaja_honorarios')->nullable();
            $table->string('maximo_grado_estudios')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datos_generales');
    }
}
