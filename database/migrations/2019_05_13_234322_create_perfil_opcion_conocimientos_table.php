<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerfilOpcionConocimientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perfil_opcion_conocimientos', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->unsignedtinyInteger('perfil_opcion_id');
            $table->foreign('perfil_opcion_id')->references('id')->on('perfil_opciones');
            $table->string('descripcion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perfil_opcion_conocimientos');
    }
}
