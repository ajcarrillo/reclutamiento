<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 2019-05-18
 * Time: 22:56
 */

namespace Authentication\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    protected $namespace = 'Authentication\Http\Controllers';

    public function boot()
    {
        parent::boot();
    }

    public function register()
    {
        $this->mapApiRoutes();
    }

    protected function mapApiRoutes()
    {
        Route::prefix('/api/auth')
            ->name('api.')
            ->namespace($this->namespace)
            ->group(base_path('app/Modules/SPA/Auth/Routes/api.php'));
    }
}
