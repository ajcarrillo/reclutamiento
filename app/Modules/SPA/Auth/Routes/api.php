<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 2019-05-18
 * Time: 22:58
 *
 * path: /api/auth
 * name: api.
 *
 */

Route::post('/register', 'RegisterController@store')->name('auth.register');
