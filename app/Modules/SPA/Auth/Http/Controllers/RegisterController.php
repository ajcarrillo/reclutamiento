<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 2019-05-18
 * Time: 23:02
 */

namespace Authentication\Http\Controllers;


use Aspirantes\Models\DatoGeneral;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Reclutamiento\Classes\Curp;
use Reclutamiento\Http\Controllers\Controller;
use Reclutamiento\User;
use Throwable;

class RegisterController extends Controller
{
    public function store(Request $request)
    {
        $this->validator($request->all())->validate();

        /*event(new Registered($user = $this->create($request->all())));*/

        $user = $this->create($request->all());

        return ok([
            'user'        => [
                'uuid'      => $user->uuid,
                'nombre'    => $user->nombre_completo,
                'email'     => $user->email,
                'api_token' => $user->api_token,
            ],
            'roles'       => $user->getRoleNames(),
            'permissions' => $user->getAllPermissions()->pluck('name'),
        ]);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nombre'          => [ 'required', 'string', 'max:255' ],
            'primer_apellido' => [ 'required', 'string', 'max:255' ],
            'email'           => [ 'required', 'string', 'email', 'max:255', 'unique:users' ],
            'password'        => [ 'required', 'string', 'min:6' ],
            'curp'            => [ 'required', 'min:18', 'max:18' ],
        ]);
    }

    protected function create(array $data)
    {
        try {
            return DB::transaction(function () use ($data) {
                $curp = new Curp($data['curp']);

                $user = User::create([
                    'nombre'           => $data['nombre'],
                    'primer_apellido'  => $data['primer_apellido'],
                    'segundo_apellido' => $data['segundo_apellido'],
                    'email'            => $data['email'],
                    'password'         => Hash::make($data['password']),
                ]);

                $generales = DatoGeneral::create([
                    'curp'               => $data['curp'],
                    'edad'               => $curp->getEdad(),
                    'fecha_nacimiento'   => $curp->getFechaNacimiento()->format('Y-m-d'),
                    'entidad_nacimiento' => $curp->getEntidadNacimiento(),
                    'sexo'               => $curp->getSexo(),
                    'user_id'            => $user->id,
                ]);

                $user->assignRole('aspirante');

                return $user;
            });
        } catch (\Exception $e) {
            \Log::info($e->getMessage());

            return $e;
        } catch (Throwable $e) {
            \Log::info($e->getMessage());

            return $e;
        }
    }
}
