<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 2019-05-27
 * Time: 11:18
 */

namespace Aspirantes\Http\Controllers;


use Aspirantes\Models\EstadoCivil;
use Reclutamiento\Http\Controllers\Controller;

class CatalogosController extends Controller
{
    public function estadoCivil()
    {
        $items = EstadoCivil::query()
            ->orderBy('descripcion')
            ->get([ 'id', 'descripcion' ]);

        return ok([ 'estados' => $items ]);
    }
}
