<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 2019-05-26
 * Time: 22:54
 */

namespace Aspirantes\Http\Controllers;


use Illuminate\Http\Request;
use Reclutamiento\Http\Controllers\Controller;

class RFCController extends Controller
{
    public function update(Request $request, $id)
    {
        $datoGeneral = $request->user()->datosGenerales()->find($id);

        $rfc = $request->only('rfc');

        $datoGeneral->update($rfc);

        return ok([ 'rfc' => $datoGeneral->rfc ]);
    }
}
