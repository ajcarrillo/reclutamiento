<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 2019-05-23
 * Time: 00:08
 */

namespace Aspirantes\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Reclutamiento\User;

class DatosGeneralesController extends Controller
{
    public function show($user)
    {
        $user = User::query()
            ->with('datosGenerales', 'datosGenerales.estadoCivil')
            ->whereUuid($user)
            ->first();

        return ok([
            'user' => $user,
        ]);
    }

    public function update(Request $request, $id)
    {
        $datosGenerales = $request->user()->datosGenerales()->find($id);

        $datosGenerales->update($request->only([
            'tipo_sangre',
            'estado_civil_id',
            'tiene_hijos',
            'numero_hijos',
            'puede_cambiar_residencia',
            'labora_actualmente',
            'aspiracion_salarial',
            'trabaja_honorarios',
            'maximo_grado_estudios',
        ]));

        return ok([ 'datos_generales' => $datosGenerales ]);
    }
}
