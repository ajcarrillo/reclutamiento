<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 2019-05-26
 * Time: 22:50
 */

namespace Aspirantes\Models;


use Illuminate\Database\Eloquent\Model;

class EstadoCivil extends Model
{
    protected $table = 'estados_civiles';
}
