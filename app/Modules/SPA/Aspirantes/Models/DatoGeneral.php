<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 2019-05-22
 * Time: 23:03
 */

namespace Aspirantes\Models;


use Illuminate\Database\Eloquent\Model;
use Reclutamiento\User;

class DatoGeneral extends Model
{
    protected $table    = 'datos_generales';
    protected $fillable = [
        'curp', 'rfc', 'edad', 'fecha_nacimiento', 'sexo', 'tipo_sangre',
        'estado_civil_id', 'tiene_hijos', 'numero_hijos', 'puede_cambiar_residencia',
        'labora_actualmente', 'aspiracion_salarial', 'trabaja_honorarios',
        'maximo_grado_estudios', 'entidad_nacimiento', 'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function estadoCivil()
    {
        return $this->belongsTo(EstadoCivil::class, 'estado_civil_id');
    }

    public function setRfcAttribute($value)
    {
        $this->attributes['rfc'] = mb_strtoupper($value, 'UTF-8');
    }
}
