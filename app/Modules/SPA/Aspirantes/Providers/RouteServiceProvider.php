<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 2019-05-23
 * Time: 00:18
 */

namespace Aspirantes\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    protected $namespace = 'Aspirantes\Http\Controllers';

    public function boot()
    {
        parent::boot();
    }

    public function register()
    {
        $this->mapApiRoutes();
    }

    protected function mapApiRoutes()
    {
        Route::prefix('/api/aspirantes')
            ->name('api.')
            ->namespace($this->namespace)
            ->group(base_path('app/Modules/SPA/Aspirantes/Routes/api.php'));
    }
}
