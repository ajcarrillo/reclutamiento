<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 2019-05-23
 * Time: 00:20
 */

// path: /api/aspirantes
// name: api.

Route::middleware('auth:api')
    ->name('aspirantes.')
    ->group(function () {
        Route::get('/{user}/datos-generales', 'DatosGeneralesController@show')->name('show');

        Route::patch('/datos-generales/{id}', 'DatosGeneralesController@update')->name('update.datos');
        Route::patch('/datos-generales/{id}/rfc', 'RFCController@update')->name('update.rfc');

        Route::name('catalogos.')
            ->prefix('/catalogos')
            ->group(function () {
                Route::get('/estados-civiles', 'CatalogosController@estadoCivil')->name('estados.civiles');
            });
    });
