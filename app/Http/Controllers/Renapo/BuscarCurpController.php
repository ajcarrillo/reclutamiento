<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 2019-05-18
 * Time: 01:29
 */

namespace Reclutamiento\Http\Controllers\Renapo;


use Reclutamiento\Classes\Curp;
use Reclutamiento\Http\Controllers\Controller;

class BuscarCurpController extends Controller
{
    public function show($curp)
    {
        $curp = new Curp($curp);

        $persona = $curp->resolverCurp();

        return ok(compact('persona'));
    }
}
