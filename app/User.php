<?php

namespace Reclutamiento;

use Aspirantes\Models\DatoGeneral;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;
use Spatie\Permission\Traits\HasRoles;
use Wildside\Userstamps\Userstamps;

class User extends Authenticatable
{
    use Notifiable, Userstamps, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'primer_apellido', 'segundo_apellido', 'email', 'password', 'nombre_completo',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function datosGenerales()
    {
        return $this->hasOne(DatoGeneral::class, 'user_id');
    }

    public function setNombreAttribute($value)
    {
        $this->attributes['nombre'] = mb_strtoupper($value, 'UTF-8');
    }

    public function setPrimerApellidoAttribute($value)
    {
        $this->attributes['primer_apellido'] = mb_strtoupper($value, 'UTF-8');
    }

    public function setSegundoApellidoAttribute($value)
    {
        $this->attributes['segundo_apellido'] = mb_strtoupper($value, 'UTF-8');
    }

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid            = Uuid::uuid4()->toString();
            $model->api_token       = Str::random(60);
            $model->active          = true;
            $model->nombre_completo = "{$model->nombre} {$model->primer_apellido} {$model->segundo_apellido}";
        });

        static::updating(function ($model) {
            $model->nombre_completo = "{$model->nombre} {$model->primer_apellido} {$model->segundo_apellido}";
        });
    }
}
