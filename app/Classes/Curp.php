<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 2019-05-18
 * Time: 01:46
 */

namespace Reclutamiento\Classes;


use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class Curp
{
    private $curp;
    private $_edad;
    private $_sexo;
    private $_fechaNacimiento;
    private $_entidadNacimiento;

    public function __construct($curp)
    {
        $this->curp = $curp;
    }

    /**
     * @return mixed
     */
    public function getCurp()
    {
        return $this->curp;
    }

    /**
     * @param mixed $curp
     */
    public function setCurp($curp): void
    {
        $this->curp = $curp;
    }

    public function resolverCurp()
    {
        $client = new Client([ 'base_uri' => env('RENAPO_URL') ]);

        try {
            $response = $client->request('POST', "ServiceAction?txtCurp={$this->curp}", [
                'form_data' => [
                    'txtCurp' => $this->curp,
                ],
            ]);

            $data = json_decode((string)$response->getBody(), true);

            return $data;
        } catch (GuzzleException $e) {
            \Log::info($e->getMessage());
            \Log::info($e->getTraceAsString());

            return $e;
        }
    }

    /**
     * @return mixed
     */
    public function getEdad()
    {
        $this->_edad = $this->getFechaNacimiento()->age;

        return $this->_edad;
    }

    /**
     * @return mixed
     */
    public function getSexo()
    {
        $this->_sexo = substr($this->curp, 10, 1);

        return $this->_sexo;
    }

    /**
     * @return mixed
     */
    public function getFechaNacimiento()
    {
        $yearCurp  = Carbon::createFromFormat('y', substr($this->curp, 4, 2))->format('Y');
        $monthCurp = substr($this->curp, 6, 2);
        $dayCurp   = substr($this->curp, 8, 2);

        //$this->_fechaNacimiento = implode("-", array_reverse(explode("/", $fecha)));

        $this->_fechaNacimiento = Carbon::createFromFormat('Y-m-d', "{$yearCurp}-{$monthCurp}-{$dayCurp}");

        return $this->_fechaNacimiento;
    }

    /**
     * @return mixed
     */
    public function getEntidadNacimiento()
    {
        $letra                    = substr($this->curp, 11, 2);
        $this->_entidadNacimiento = trans("magic_numbers.entidad_por_letra.{$letra}");

        return $this->_entidadNacimiento;
    }
}

