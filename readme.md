# Reclutamiento

# Contribuir

:+1: :tada: Primero que nada gracias por tomarte el tiempo para 
contribuir a este proyecto :+1: :tada:

### Antes de hacer tu contribución por favor lee la guía de estilo del código [Guía de estilo del código](https://gitlab.com/ajcarrillo/guia_desarrollo/).

El estilo del código es especialmente importante si estamos en un equipo 
de desarrollo o si nuestro proyecto lo van a usar en algún momento otros 
desarrolladores. Pero, cuando trabajamos en un proyecto propio, también 
es una buena costumbre usar un estilo de código claro y optimizado. 
Nos ayudará a revisar mejor el código y a entenderlo si en algún momento 
tenemos que modificarlo o queremos reutilizarlo.

Para contribuir a este proyecto sigue los siguientes pasos:
 
 ```
 $ git clone https://gitlab.com/ajcarrillo/reclutamiento.git
 $ git checkout -b devTuNombre
 
 // Despues de hacer tu contribución
 $ git pull origin master
 // Corrige conflictos, si los hay
 $ git push origin devTuNombre
 ```
 
 Crea tu merge request en el sitio de gitlab.
 
 * Clona el proyecto
 * Crea tu rama de trabajo
 * Haz tu contribución
 * Baja los últimos cambios en la rama `master`, arregla conflictos si los hay
 * Sube tu contribución
 * Y haz un merge request a la rama `master` del proyecto.

## :baby_bottle: Baby Steps I

* Copiar y renombrar el archivo `/.env.example` a `.env`

### Archivo .env

Aquí se encuentra contenidas algunas variables y configuraciones de vital 
importancia para que el proyecto funcione.

La variable `APP_KEY` se rellena automáticamente ejecutando el comando:

```
$ php artisan key:generate
```

## :baby_bottle: Baby Steps II

### Configuración y creacion de bases de datos.

Puedes crear las bases de datos ejecutando los siguientes scripts:

Base de datos `reclutamiento`:
```sql
DROP DATABASE IF EXISTS reclutamiento;
CREATE DATABASE reclutamiento
   CHARACTER SET = 'utf8mb4'
   COLLATE = 'utf8mb4_unicode_ci';
```

## Composer packages

### [Ide helper](https://github.com/barryvdh/laravel-ide-helper)

> `bootstrap/compiled.php` has to be cleared first, so run `php artisan clear-compiled` before generating. 

```
$ php artisan ide-helper:generate
$ php artisan ide-helper:meta
```

### [Laravel telescope](https://laravel.com/docs/5.8/telescope#installation)

After installing Telescope, publish its assets using the `telescope:install` 
Artisan command. After installing Telescope, you should also run the `migrate` command:

```
$ php artisan telescope:install
$ php artisan migrate
```

#### Migration Customization

If you are not going to use Telescope's default migrations, you should 
call the  `Telescope::ignoreMigrations` method in the `register` method of 
your `AppServiceProvider`. You may export the default migrations using the 
`php artisan vendor:publish --tag=telescope-migrations` command.

#### Dashboard Authorization

Telescope exposes a dashboard at `/telescope`. By default, you will only 
be able to access this dashboard in the `local` environment. Within your 
`app/Providers/TelescopeServiceProvider.php` file, there is a `gate` method. 
This authorization gate controls access to Telescope in **non-local** 
environments. You are free to modify this gate as needed to restrict 
access to your Telescope installation:

```php
/**
 * Register the Telescope gate.
 *
 * This gate determines who can access Telescope in non-local environments.
 *
 * @return void
 */
protected function gate()
{
    Gate::define('viewTelescope', function ($user) {
        return in_array($user->email, [
            'user.email@sample.com',
        ]);
    });
}
```

### [Laravel query detector](https://github.com/beyondcode/laravel-query-detector)

> For more information go to the package documentation.

### [Laravel permission](https://github.com/spatie/laravel-permission)

> For more information go to the package documentation.
