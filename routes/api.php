<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    $user = $request->user();

    return ok([
        'user'        => [
            'uuid'      => $user->uuid,
            'nombre'    => $user->nombre_completo,
            'email'     => $user->email,
            'api_token' => $user->api_token,
        ],
        'roles'       => $user->getRoleNames(),
        'permissions' => $user->getAllPermissions()->pluck('name'),
    ]);
})->name('api.user');

Route::get('/resolver-curp/{curp}', 'Renapo\BuscarCurpController@show')->name('api.curp');
